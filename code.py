# filename code.py
# RFID reader for raspberry that communicates with server 
# author Fredrik Lindseth    
# date created  2016-11-3  
# date modified 2017-11-19              
# version 0.2                 
# Tools: Python 3.5.3, [GCC 6.3.0 20170406] on linux

import json
import time #for delay/sleep

import serial #for serial communication
import RPi.GPIO as GPIO #Raspberry GPIO library
import requests #for http requests


config = json.load(open('config.json'))
auth_token = config['jwt']
url = config['host']
headers = {'Authorization': auth_token}

# set up serial communication
serial = serial.Serial()
serial.baudrate = 9600
serial.port = "/dev/ttyUSB0"

# debug info
print(serial)
print("RPi.GPIO version:"+GPIO.VERSION)

# open the serial port
serial.open()

# to use Raspberry pi board pin numbers
GPIO.setmode(GPIO.BOARD)

# disable runtime warnings
GPIO.setwarnings(False)

def main():
    data = ''

    # set up GPIO output channels
    GPIO.setup(11, GPIO.OUT, initial=GPIO.LOW) # red LED in the RGB
    GPIO.setup(13, GPIO.OUT, initial=GPIO.LOW) # green LED in the RGB
    GPIO.setup(15, GPIO.OUT, initial=GPIO.LOW) # blue LED in the RGB

    print("Blinking LEDS to show they're alive")
    GPIO.output(11, GPIO.HIGH)
    time.sleep(1)
    GPIO.output(11, GPIO.LOW)
    GPIO.output(13, GPIO.HIGH)
    time.sleep(1)
    GPIO.output(13, GPIO.LOW)
    GPIO.output(15, GPIO.HIGH)
    time.sleep(1)
    GPIO.output(15, GPIO.LOW)
    print("Done blinking")

    # set the "powered on"-LED to high
    GPIO.output(15, GPIO.HIGH)

    try:
        while True:
            data = serial.readline()

            # strip \r\n from end of line
            data = data.strip()
            # strip \x02
            data = data.replace("\x02", "")
            # strip \x03
            data = data.replace("\x03", "")

            print("data read="+repr(data)+"-END")

            r = requests.get(url+str(data), headers=headers)

            if r.status_code == 200:
                    print("Beer")

                    # blink green LED
                    GPIO.output(15, GPIO.LOW)
                    GPIO.output(13, GPIO.HIGH)
                    time.sleep(2)
                    GPIO.output(13, GPIO.LOW)
                    GPIO.output(15, GPIO.HIGH)

            #e lse ikke gyldig r.status_code == 422
            else:
                    print("No beer for you ")
                    # blink red LED
                    GPIO.output(15, GPIO.LOW)
                    GPIO.output(11, GPIO.HIGH)
                    time.sleep(2)
                    GPIO.output(11, GPIO.LOW)
                    GPIO.output(15, GPIO.HIGH)
            data = ''

            # delays for 0.5 seconds so the loop doesn't run so fast
            time.sleep(0.5)

    except KeyboardInterrupt:
        print("\nKeyboardInterrupt")
        GPIO.cleanup()

    except Exception:
         print ("Other error or exception occurred! \n " + str(e))
    finally:
         GPIO.cleanup()

if __name__=="__main__":
    main()
