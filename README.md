# RFID reader for raspberry that communicates with server

This snipppet of Python code is to be used with an RFID reader (ID-12LA (125 kHz)) and a Raspberry Pi.
The raspberry reads the RFID, communicates with a backend which checks the ID against a list of paying customers and then gives a visual and auditory response.

This was used for a payment system that used Vipps for payment and a system in the backend that allowed users to order items via an webpage, and another webpage for the people behind the counter, to verify that customers had paid.

## Setup
* Be default it an USB mounted on /dev/ttyUSB0, change this to your USB port of choice.
* To install required python packages listed in [requirements.txt](requirements.txt) run `pip install -r requirements.txt`
* If you are connecting to a backend you have to set the host and the JSON Web Token in [config.json](config.json)
